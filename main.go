package main

import (
	"context"
	"encoding/json"
	"fmt"
	"github.com/docker/docker/api/types"
	"github.com/docker/docker/api/types/filters"
	"github.com/docker/docker/client"
	"github.com/jackc/pgx/v4"
	"github.com/labstack/echo/v4"
	log "github.com/sirupsen/logrus"
	"os"
	"os/signal"
	"strings"
	"time"
)

// Api Small api struct to inject the db for the http calls
type Api struct {
	db *pgx.Conn
}

func main() {
	// initDockerCollector as a goroutine because we don't want to run the data collection in the same "thread"
	// as the Api
	// could/should be inside a work in a separate concern
	go initDockerCollector()

	initApi()
}

// metrics GET /metrics http handler
func (api *Api) metrics(c echo.Context) error {
	filterMetric := c.Param("metric")
	var rows pgx.Rows
	if filterMetric == "" {
		rows, _ = api.db.Query(context.Background(), "select raw_stats from stats")
	} else {
		// here is where I would handle the metric type(cpu/mem/timestamp) + filter type
		// Mapping filter type (over, before, under, after etc...) to sql expression in our case
	}

	var stats strings.Builder
	// fetch and return only the raw stats
	for rows.Next() {
		var theStats string
		err := rows.Scan(&theStats)
		if err != nil {
			return err
		}
		stats.WriteString(theStats + "\n")
	}
	return c.String(200, stats.String())
}

// initApi setup the rest api routes and start said api on :8001
func initApi() {
	// channel to listen for events like ctrl-c or other possible exiting events
	done := make(chan bool)
	quit := make(chan os.Signal)
	signal.Notify(quit, os.Interrupt)

	db, err := NewPgxDB()
	if err != nil {
		log.Fatal(err)
	}

	api := &Api{db: db}
	e := echo.New()

	// define router endpoints
	e.GET("/metrics", api.metrics)
	e.GET("/metrics/:metric", api.metrics)

	go func() {
		<-quit
		log.Info("Server is shutting down...")

		ctx, cancel := context.WithTimeout(context.Background(), 30*time.Second)
		defer cancel()

		if err := e.Shutdown(ctx); err != nil {
			log.Fatalf("Could not gracefully shutdown the server: %v\n", err)
		}
		close(done)
	}()

	log.Panic(e.Start(":8001"))
}

//
func initDockerCollector() {
	ticker := time.NewTicker(10 * time.Second)
	quit := make(chan struct{})
	c := make(chan os.Signal)
	signal.Notify(c, os.Interrupt)

	for {
		select {
		//Using a ticker instead of time.After
		//because time.After allocate a new timer every iteration
		//and we care about keeping the task on schedule I.E every 10secs
		case <-ticker.C:
			processContainer()
		case <-quit:
			log.Info("monitoring shutting down")
			ticker.Stop()
			return
		case sig := <-c:
			log.Infof("Got %s signal. Aborting...\n", sig)
			ticker.Stop()
			return
		}
	}
}

func processContainer() {
	cli, err := client.NewClientWithOpts(client.FromEnv)
	if err != nil {
		panic(err)
	}
	// Only list our test case for now
	args := filters.NewArgs(filters.KeyValuePair{
		Key:   "name",
		Value: "/stress_test",
	})
	containers, err := cli.ContainerList(context.Background(), types.ContainerListOptions{Filters: args, Limit: 1})
	if err != nil {
		panic(err)
	}
	if len(containers) == 0 {
		panic("container not found")
	}
	container := containers[0]
	if container.State == "running" {
		collect(container, cli)
	}
}

type SimpleMetrics struct {
	Ts        int64
	IPAddress string
	Cpu       float64
	Mem       float64
	Raw       string
}

func collect(container types.Container, cli *client.Client) {
	db, err := NewPgxDB()
	if err != nil {
		log.Fatal(err)
	}
	// hardcoded default network name
	networkBridge := container.NetworkSettings.Networks["dockerstats_default"]
	ipAddress := networkBridge.IPAddress
	targetedContainerID := container.ID
	cName := container.Names[0]

	if stats, err := cli.ContainerStats(context.Background(), targetedContainerID, false); err != nil {
		log.Fatal(err)
	} else {
		var containerStats containerStats
		err := json.NewDecoder(stats.Body).Decode(&containerStats)
		if err != nil {
			log.Error("can't read api stats: ", err)
		}
		if err := stats.Body.Close(); err != nil {
			log.Error("can't close body: ", err)
		}
		// metrics in this simple format (timestamp | machine ip | cpu% | ram usage)
		// for the sake the test we'll use a simple string builder
		// should be pushing to a prometheus channel for better data collector
		var sb strings.Builder
		var simpleMetrics SimpleMetrics
		simpleMetrics.Ts = time.Now().Unix()
		simpleMetrics.IPAddress = ipAddress
		sb.WriteString(fmt.Sprintf("timestamp:%d|", simpleMetrics.Ts))
		sb.WriteString(fmt.Sprintf("IPAddress:%s|", simpleMetrics.IPAddress))
		memoryMetrics(&simpleMetrics, &sb, &containerStats, cName)
		cPUMetrics(&simpleMetrics, &sb, &containerStats, cName)
		simpleMetrics.Raw = sb.String()
		log.Println(simpleMetrics.Raw)
		persistMetrics(context.Background(), db, simpleMetrics)

	}
}

func cPUMetrics(simpleMetrics *SimpleMetrics, builder *strings.Builder, containerStats *containerStats, cName string) {
	cpuDelta := containerStats.CPU.CPUUsage.TotalUsage - containerStats.PreCPU.CPUUsage.TotalUsage
	sysemDelta := containerStats.CPU.SystemCpuUsage - containerStats.PreCPU.SystemCpuUsage

	cpuUtilization := float64(cpuDelta) / float64(sysemDelta) * 100.0
	simpleMetrics.Cpu = cpuUtilization
	builder.WriteString(fmt.Sprintf("cpu:%.2f|", cpuUtilization))

}

func memoryMetrics(simpleMetrics *SimpleMetrics, builder *strings.Builder, containerStats *containerStats, cName string) {
	// From official documentation
	//Note: On Linux, the Docker CLI reports memory usage by subtracting page cache usage from the total memory usage.
	//The API does not perform such a calculation but rather provides the total memory usage and the amount from the page cache so that clients can use the data as needed.
	memoryUsage := containerStats.Memory.Usage - containerStats.Memory.MemoryStats.Cache

	memoryUtilization := float64(memoryUsage) / float64(containerStats.Memory.Limit) * 100.0
	simpleMetrics.Mem = memoryUtilization
	builder.WriteString(fmt.Sprintf("memUsage:%.2f|", memoryUtilization))
}
