start:
	go run .

stress:
	docker-compose up progrium

run-timescale:
	docker-compose up  -d timescaledb

setup-db: run-timescale
	go run ./cmd createdb
	go run ./cmd createTable
	docker-compose stop timescaledb