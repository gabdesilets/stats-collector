package main

import (
	"context"
	"fmt"
	"github.com/jackc/pgx/v4"
	log "github.com/sirupsen/logrus"
)

//NewPgxDB Get a new PGX connection
func NewPgxDB() (*pgx.Conn, error) {
	var connStr = "postgres://postgres:toor@localhost:5432/metrics"
	ctx := context.Background()
	conn, err := pgx.Connect(ctx, connStr)
	if err != nil {
		return nil, fmt.Errorf("pgx connection error: %w", err)
	}
	if err := conn.Ping(ctx); err != nil {
		return nil, fmt.Errorf("pgx ping error: %w", err)
	}
	return conn, nil
}

// persistMetrics save into timescaledb a poor man version of the metrics
// for the sake of speed and the exercise we assume nothing fail...
// not a real life case
func persistMetrics(ctx context.Context, db *pgx.Conn, simpleMetrics SimpleMetrics) {
	_, err := db.Exec(
		ctx,
		"insert into stats(ts, cpu, mem, raw_stats, ip_address) values($1, $2, $3, $4, $5)",
		simpleMetrics.Ts, simpleMetrics.Cpu, simpleMetrics.Mem, simpleMetrics.Raw, simpleMetrics.IPAddress)
	if err != nil {
		log.Error(err)
		return
	}
}
