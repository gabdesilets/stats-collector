package main

import (
	"context"
	"github.com/jackc/pgx/v4"
	log "github.com/sirupsen/logrus"
	"os"
)

// This should only be run for the setup
// should go into a migration system, but will do for now

// Simple task run for database setup
// the Args is whether we want to create the DB or table
func main() {
	cmd := os.Args[1]
	switch cmd {
	case "createdb":
		createDB()
	case "createTable":
		createStatsTable()
	default:
		log.Error("invalid command:", cmd)
	}
}

// createStatsTable simple function that create the stats table and the indexes
func createStatsTable() {

	// run create table + create index one shot for the sake of the test instead of
	// 4 separated statements
	var createStatsTable = `
		CREATE TABLE IF NOT EXISTS stats (
			ts bigint not null,
			cpu numeric not null,
			mem numeric not null,
			raw_stats varchar(100) not null,
			ip_address varchar(50) not null
		); 
		CREATE INDEX index_ts
			ON stats (ts);	
		CREATE INDEX index_cpu
			ON stats (cpu);
		CREATE INDEX index_mem
			ON stats (mem);
	`
	var connStr = "postgres://postgres:toor@localhost:5432/metrics"
	ctx := context.Background()
	conn, err := pgx.Connect(ctx, connStr)
	if err != nil {
		log.Error(err)
		return
	}
	if err := conn.Ping(ctx); err != nil {
		panic(err)
	}
	_, err = conn.Exec(ctx, createStatsTable)
	if err != nil {
		log.Error(err)
		return
	}
	log.Info("stats table created")

}

// createDB create the metrics database
func createDB() {
	var createDBQuery = `
	CREATE DATABASE metrics;
`
	var connStr = "postgres://postgres:toor@localhost:5432/"
	ctx := context.Background()
	conn, err := pgx.Connect(ctx, connStr)
	if err != nil {
		panic(err)
	}
	if err := conn.Ping(ctx); err != nil {
		log.Error(err)
		return
	}
	_, err = conn.Exec(ctx, createDBQuery)
	if err != nil {
		log.Error(err)
		return
	}
	log.Info("metrics DB created")
}
