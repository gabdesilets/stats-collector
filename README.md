## Gabriel Personal notes to whom this concern

## Requirements

- Go v1.17
- Docker
- docker-compose
- make

## Run the project

- run `make setup-db`
- start the db `make run-timescale`
- start api and collector `make start`
- run stress test `make stress`

### Internal release only

In the case we don't want them public could always build the module itself with `go build -o bin/dockerstats` install the binary
into the `$GOPATH/bin`

## Alternative solution
Initially I would have only used these technologies with minimal or even no `go` code.

- [cadvisor](https://github.com/google/cadvisor) - provides container users an understanding of the resource usage and performance characteristics of their running containers.
- [grafana](https://grafana.com/) - as the web dashboard for the metrics
- [prometheus](https://github.com/prometheus/prometheus) - as the collector

With this `docker-compose` setup
```yml
version: '3'

  progrium:
      image: progrium/stress
      container_name: stress_test
      command: [--cpu, '8', --io, '4', --vm, '2', --vm-bytes, '128M', --timeout, '30s']

  cadvisor:
    image: gcr.io/cadvisor/cadvisor:latest
    container_name: cadvisor
    ports:
      - "8080:8080"
    volumes:
      - /:/rootfs:ro
      - /var/run:/var/run:rw
      - /sys:/sys:ro
      - /var/lib/docker/:/var/lib/docker:ro
      - /var/run/docker.sock:/var/run/docker.sock:ro
      - /cgroup:/cgroup:ro

  grafana:
    image: grafana/grafana-oss
    container_name: grafana
    volumes:
      - grafana_vol:/var/lib/grafana
      - ./grafana/provisioning/dashboards:/etc/grafana/provisioning/dashboards
      - ./grafana/provisioning/datasources:/etc/grafana/provisioning/datasources
    ports:
      - "3000:3000"
    environment:
      - GF_LOG_LEVEL=error
  prometheus:
    image: prom/prometheus
    container_name: prometheus
    command:
      - --config.file=/etc/prometheus/prometheus.yml
    volumes:
      - prom-volume:/prometheus
      - ./prometheus:/etc/prometheus
    ports:
      - 9090:9090

  node-exporter:
    image: prom/node-exporter
    container_name: node_exporter
    volumes:
      - /proc:/host/proc:ro
      - /sys:/host/sys:ro
      - /:/rootfs:ro
    command:
      - '--path.procfs=/host/proc'
      - '--path.sysfs=/host/sys'
    ports:
      - 9100:9100
volumes:
    prom-volume:
    grafana_vol:
```

with prometheus `scrape_interval` every 10sec.
